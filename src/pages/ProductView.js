import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import productCard2 from "../multimedia/productCard2.jpg";

export default function ProductView(){

	const { user } = useContext(UserContext);

    const [userId, setUserId] = useState(localStorage.getItem("userId"));
	console.log(userId);

	// "useParams" hooks allows us to retrieve the courseId passed via the URL.

	const { productId } = useParams();

	const navigate = useNavigate();

	// Create state hooks to capture the information of a specific course and display it in our application.
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
    const [stock, setStock] = useState(0);

	useEffect(()=>{
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
            setStock(data.stock);

		});

	}, [productId])

	const checkout = (productId) =>{

		fetch(`${ process.env.REACT_APP_API_URL }/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log("Checkout data: ")
			console.log(data);

			if(data){
				Swal.fire({
					title: "Checkout Successfull",
					icon: "success",
					text: "You have successfully checked out."
				});

				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Container>
			<Row id='productCard'>
				<Col lg={{ span: 6, offset: 3}}>
					<Card className="m-5">
						<Card.Body className="text-center p-5 bg-dark">
                            <div id="productContent">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Stock:</Card.Subtitle>
							<Card.Text>{stock}</Card.Text>
                            </div>
							{
								(user.id !== null || userId !== null)
								?
									<Button variant="danger"  size="lg" onClick={() => checkout(productId)}>Checkout</Button>
								:
									<Button as={Link} to="/login" variant="danger"  size="lg">Login to Checkout</Button>
							}
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
