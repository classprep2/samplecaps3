import { Form, Button, Container, Col, Row } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from "sweetalert2";

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	//State hooks to store the values of input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfully binded
	// console.log(firstName);
	// console.log(lastName);
	// console.log(email);
	// console.log(password1);
	// console.log(password2);
	// console.log(mobileNumber);



function registerUser(event){

    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
        method: "POST",
        headers:{
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password1,
            mobileNo: mobileNumber
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        if(data){
            Swal.fire({
                title: "Registration Successful",
                icon: "success",
                text: "Itadakimasu!"
            });

            // Clear input fields
            setFirstName('');
            setLastName('');
            setEmail('');
            setMobileNumber('');
            setPassword1('');
            setPassword2('');

            // Allow us to redirect the user to the login page after account registration
            navigate("/login");
        }
        else{

            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text: "Please try again."
            });

        }
    })

		
}
	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNumber !== '') && (password1 === password2)){setIsActive(true);}
		else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2, mobileNumber])


	return(

		
		(user.id !== null)
		?// true - means email field is successfully set
		<Navigate to="/products" />
		: // false - means email field is not successfully set
        <div id='registerContainer'>
		<Form id='registerInput' onSubmit ={(event) => registerUser(event)}>
		<h3>Register</h3>
			<Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter First Name" 
	                value = {firstName}
	                onChange = {event => setFirstName(event.target.value)}
	                required
                />
            </Form.Group>

			<Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter Last Name" 
	                value = {lastName}
	                onChange = {event => setLastName(event.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value = {email}
	                onChange = {event => setEmail(event.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value = {password1}
	                onChange = {event => setPassword1(event.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                value = {password2}
	                onChange = {event => setPassword2(event.target.value)}
	                required
                />
            </Form.Group>

			<Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="number" 
	                placeholder="Enter Mobile Number" 
	                value = {mobileNumber}
	                onChange = {event => setMobileNumber(event.target.value)}
	                required
                />
            </Form.Group><br/>
            { isActive ? // true or if
	            <Button variant="primary"  size="lg" type="submit" id="submitBtn">
	            	Submit
	            </Button>
	            : // false or else
	            <Button variant="secondary" size="lg" type="submit" id="submitBtn" disabled>
            	Submit
            	</Button>

            }
            
        </Form>
        </div>

	)
}