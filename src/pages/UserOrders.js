import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function UserOrders(){

	const { user } = useContext(UserContext);

    const [allOrders, setAllOrders] = useState('');
    // const [userId, setUserId] = useState(localStorage.getItem("userId"));
	// console.log(userId);

	// "useParams" hooks allows us to retrieve the courseId passed via the URL.

	const { userId } = useParams();

	const navigate = useNavigate();

	// Create state hooks to capture the information of a specific course and display it in our application.
    const [name, setName] = useState('');
	const [purchasedOn, setPurchasedOn] = useState('');
	const [productName, setProductName] = useState('');
	const [quantity, setQuantity] = useState(0);
    
	useEffect(()=>{
		console.log(userId);

		fetch(`${ process.env.REACT_APP_API_URL }/users/order/${userId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

            setAllOrders(data.map(order => {
                // console.log(order);
                return(
                    <Container>
                        <Row id='ordersCard'>
                            <Col lg={{ span: 6, offset: 3}}>
                                <Card className="m-5">
                                    <Card.Body className="text-center p-5 bg-dark text-light">
                                        <div id="ordersContent">
                                        <Card.Title>{order.name}</Card.Title>
                                        <Card.Subtitle>Purchased On:</Card.Subtitle>
                                        <Card.Text>{order.purchasedOn}</Card.Text>
                                        <Card.Subtitle>Product Id:</Card.Subtitle>
                                        <Card.Text>{order._id}</Card.Text>
                                        </div>
                                        {
                                                <Button as={Link} to="/admin/userDetails" variant="danger"  size="lg">Back</Button>
                                        }
                                    </Card.Body>		
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                )
            }))
                
		});

	}, [userId])

	return(
        <div>
            {allOrders}
		
        </div>
	)
}