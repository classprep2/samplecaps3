import { Fragment, React } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights' ;
import home1 from '../multimedia/home1.mp4';



export default function Home() {

	const data = {
		title: "The DONburi",
		content: "The DON of All Buris",
		destination: "/products",
		label: "Order now!"
	}

	return (
        <div id='home'>
            <video id='homeVideo' src={home1} autoPlay loop muted/>
            <Fragment>
                
                <Banner data={data}/>
                <Highlights />
            </Fragment>
        </div>
	)
}
