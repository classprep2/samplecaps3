import { Row, Col, Card } from 'react-bootstrap';


// export a function
export default function Highlights() {
    return (
    	
        <div id='highlight'>
        <Row className="mt-3 mb-3 text-bottom">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 bg-transparent">
                    <Card.Body>
                        <Card.Title>
                            <h2>You Don't Want to Miss This!</h2>
                        </Card.Title>
                        <Card.Text>
                            Always check our website for you to be updated with our latest discounts and promotions.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 bg-transparent">
                    <Card.Body>
                        <Card.Title>
                            <h2>True Japanese Food Experience</h2>
                        </Card.Title>
                        <Card.Text>
                            Experience a true taste of donburis here with us because we only choose the freshest and authentic Japanese ingredients that we all love.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3 bg-transparent">
                    <Card.Body>
                        <Card.Title>
                            <h2>Be Part of Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Be a part of an active and friendliest community and share your great experiences with The DONburi.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        </div>
    )
}

