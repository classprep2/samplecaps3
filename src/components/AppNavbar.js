import {Container, Navbar, Nav} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom';
import {useState, Fragment, useContext} from 'react'; //
import UserContext from "../UserContext"

export default function AppNavbar(){
	

	const { user, setUser } = useContext(UserContext);
	console.log(user);


	const [userId, setUserId] = useState(localStorage.getItem("userId"));
	console.log(userId);
	
	
	return(
		<Navbar bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand id='navbarLogo' as={Link} to="/">The DONburi</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
		        <Navbar.Collapse id="basic-navbar-nav">
		            <Nav className="mr-auto">
			            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
			            <Nav.Link as={NavLink} to="/products" >Menu</Nav.Link>
			            {(user.id === null && userId === null)
							?
			            	<Fragment>
					            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
					            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
				            </Fragment>
			            	:
			            	<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>			            
			        	}
		            </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
}
