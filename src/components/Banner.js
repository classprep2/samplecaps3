import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){

	console.log(data);
	const {title, content, destination, label} = data;

	return (
		<Row>
			<Col className="banner">
				<h1 className='homeTitle'>{title}</h1>
				<p className='homeContent'>{content}</p>
				<Link className='homeLink' to={destination}>{label}</Link>
			</Col>
		</Row>

	)


}
