import './App.css';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';

import { UserProvider } from './UserContext'

import AdminDashboard from './pages/AdminDashboard';
import UserDetails from './pages/UserDetails';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Product from './pages/Product';
import ProductView from './pages/ProductView';
import Logout from './pages/Logout';
import Error from './pages/Error'; 
import UserOrders from './pages/UserOrders';


function App() {
  const [user,setUser] = useState({
    id: localStorage.getItem('id')
  })

  const unsetUser = () => {
    localStorage.clear();
      // setUser({
      //   id: null,
      //   isAdmin: null
      // });
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id !== undefined){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
    }
    else {
      setUser({
        id: null,
        isAdmin: null
      });
    }
    })
  },[])
  return (

    <UserProvider value = {{user,setUser, unsetUser}}> 
    <Router>
      <Container fluid>
        <AppNavbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/admin" element={<AdminDashboard />} />
          <Route exact path="/admin/userDetails" element={<UserDetails />} />
          <Route exact path="users/order/:userId" element={<UserOrders />} />

          <Route path='/register' element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route exact path="/logout" element={<Logout />} />
          <Route exact path="/products" element={<Product />} />
          <Route exact path="/products/:productId" element={<ProductView />} />
          <Route exact path="*" element={<Error />} /> 

        </Routes>       
      </Container>
    </Router>
    </UserProvider >

    
  );
}

export default App;